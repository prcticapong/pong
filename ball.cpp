//Include our classes
#include "ball.h"

Ball::Ball() {
	mpRect.x = 0;
	mpRect.y = 0;
	mpRect.h = 0;
	mpRect.w = 0;
	mpSpeedX = 0;
	mpSpeedY = 0;
	mpColor = ofColor(243, 218, 011);
}

Ball::Ball(int x, int y, int h, int w) :Ball(){
	setXY(x, y);
	setW(w);
	setH(h);
}

Ball::~Ball() {//Solo tiene que tener cosas si hay atributos que canvien de tama�o, los atributos constantes no.

}

void Ball::update() {
	mpRect.y = mpRect.y + mpSpeedY * (int)global_delta_time / 1000;
	mpRect.x = mpRect.x + mpSpeedX * (int)global_delta_time / 1000;
	return;
}

void Ball::render() {
	ofSetColor(mpColor);
	ofDrawRectangle(mpRect.x, mpRect.y, mpRect.h, mpRect.w);
	return;
}

//Setters and getters
void Ball::setRect(C_Rectangle a_rect) {
	setXY(a_rect.x, a_rect.y);
	setW(a_rect.w);
	setW(a_rect.h);
	return;
}
void Ball::setXY(int x, int y) {
	setX(x);
	setY(y);
	return;
}
void Ball::setX(int x) {
	mpRect.x = x;
	return;
}
void Ball::setY(int y) {
	mpRect.y = y;
	return;
}
void Ball::setW(int w) {
	mpRect.w = w;
	return;
}
void Ball::setH(int h) {
	mpRect.h = h;
	return;
}

//Gets
C_Rectangle Ball::getRect() {

	return mpRect;
}
int Ball::getX() {

	return mpRect.x;
}
int Ball::getY() {

	return mpRect.y;
}
int Ball::getW() {

	return mpRect.w;
}
int Ball::getH() {

	return mpRect.h;
}



void Ball::setSpeedX(int speed_x) {
	mpSpeedX = speed_x;
	return;
}
void Ball::setSpeedY(int speed_y) {
	mpSpeedY = speed_y;
	return;
}

void Ball::invertSpdX() {
	mpSpeedX = -mpSpeedX;
	return;
}

void Ball::invertSpdY() {
	mpSpeedY = -mpSpeedY;
	return;
}

int Ball::getSpeedX() {

	return mpSpeedX;
}
int Ball::getSpeedY() {

	return mpSpeedY;
}

void Ball::setColor(ofColor color){
	mpColor = color;
	return;
}


