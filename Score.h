#ifndef SCORE_H
#define SCORE_H

#include "includes.h"
#include "ofMain.h"

class Score
{
	public:
		Score();
		Score(int x, int y,int h, int w);
		~Score();

	
		void update();
		void render();

		//Setters and getters
		void setRect(C_Rectangle a_rect);
		void setXY(int x, int y);
		void setX(int x);
		void setY(int y);
		void setW(int w);
		void setH(int h);

		C_Rectangle getRect();
		int getX();
		int getY();
		int getW();
		int getH();

		void setSpeed(int Speed);
		int getSpeed();

		void setColor(ofColor color);
		void setValue(int num);

	protected:
	
	private:
		C_Rectangle mpRect; //Atributo rectangulo de la pala
		int mpSpeed;        //Velocidad de la pala
		ofColor mpColor;      //Color de la pala
		ofTrueTypeFont* mpFont;	//Mostrar por pantalla el ganador
		ofTrueTypeFont* mpFont2;
		int number;	    //Muestra la puntuación
	
};

#endif
