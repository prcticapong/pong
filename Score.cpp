//Include our classes
#include "Score.h"
#include "Utils.h"

Score::Score() {
	mpRect.x = 0;
	mpRect.y = 0;
	mpRect.h = 0;
	mpRect.w = 0;
	mpSpeed = 0;
	mpColor = ofColor(254, 0, 0);

	mpFont = new ofTrueTypeFont();
	mpFont2 = new ofTrueTypeFont();
	mpFont->loadFont("coders_crux.ttf", 60);	//Tipo de letra y tama�o
	mpFont2->loadFont("coders_crux.ttf", 60);

	number = 0;
}

Score::Score(int x, int y, int h, int w) :Score() {
	setXY(x, y);
	setW(w);
	setH(h);
}

Score::~Score() {  //Solo tiene que tener cosas si hay atributos que canvien de tama�o, los atributos constantes no.

}

void Score::update() {
	mpRect.y = mpRect.y + mpSpeed * (int)global_delta_time / 1000;
	if (mpRect.y < 0)
	{
		mpRect.y = 0;
	}
	if (mpRect.y + mpRect.h > SCREEN_HEIGHT)
	{
		mpRect.y = SCREEN_HEIGHT - mpRect.h;
	}
	return;
}

void Score::render() {
	ofSetColor(0, 255, 0);
	//ofDrawRectangle(mpRect.x, mpRect.y, mpRect.w, mpRect.h);
	mpFont->drawString(itos(number, 2), mpRect.x, mpRect.y);
	ofSetColor(255, 69, 0);
	return;
}

void Score::setValue(int num) {
	number = num;
	return;
}

//Setters and getters
void Score::setRect(C_Rectangle a_rect) {
	setXY(a_rect.x, a_rect.y);
	setW(a_rect.w);
	setH(a_rect.h);
	return;
}
void Score::setXY(int x, int y) {
	setX(x);
	setY(y);
	return;
}
void Score::setX(int x) {
	mpRect.x = x;
	return;
}
void Score::setY(int y) {
	mpRect.y = y;
	return;
}
void Score::setW(int w) {
	mpRect.w = w;
	return;
}
void Score::setH(int h) {
	mpRect.h = h;
	return;
}

//Gets
C_Rectangle Score::getRect() {

	return mpRect;
}
int Score::getX() {

	return mpRect.x;
}
int Score::getY() {

	return mpRect.y;
}
int Score::getW() {

	return mpRect.w;
}
int Score::getH() {

	return mpRect.h;
}

void Score::setSpeed(int speed) {
	mpSpeed = speed;
	return;
}
int Score::getSpeed() {

	return mpSpeed;
}

void Score::setColor(ofColor color){
	mpColor = color;
	return;
}


