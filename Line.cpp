//Include our classes
#include "Line.h"

Line::Line() {
	mpRect.x = 0;
	mpRect.y = 0;
	mpRect.h = 0;
	mpRect.w = 0;
	mpSpeed = 0;
	mpColor = ofColor(120, 133, 139);
}

Line::Line(int x, int y, int h, int w) :Line(){
	setXY(x, y);
	setW(w);
	setH(h);
}

Line::~Line() {//Solo tiene que tener cosas si hay atributos que canvien de tama�o, los atributos constantes no.

}

void Line::update() {
	mpRect.y = mpRect.y + mpSpeed * (int)global_delta_time / 1000;
	if (mpRect.y < 0)
	{
		mpRect.y = 0;
	}
	if (mpRect.y + mpRect.h > SCREEN_HEIGHT)
	{
		mpRect.y = SCREEN_HEIGHT - mpRect.h;
	}
	return;
}

void Line::render() {
	ofSetColor(mpColor);
	ofDrawRectangle(mpRect.x, mpRect.y, mpRect.w, mpRect.h);
	return;
}

//Setters and getters
void Line::setRect(C_Rectangle a_rect) {
	setXY(a_rect.x, a_rect.y);
	setW(a_rect.w);
	setH(a_rect.h);
	return;
}
void Line::setXY(int x, int y) {
	setX(x);
	setY(y);
	return;
}
void Line::setX(int x) {
	mpRect.x = x;
	return;
}
void Line::setY(int y) {
	mpRect.y = y;
	return;
}
void Line::setW(int w) {
	mpRect.w = w;
	return;
}
void Line::setH(int h) {
	mpRect.h = h;
	return;
}

//Gets
C_Rectangle Line::getRect() {

	return mpRect;
}
int Line::getX() {

	return mpRect.x;
}
int Line::getY() {

	return mpRect.y;
}
int Line::getW() {

	return mpRect.w;
}
int Line::getH() {

	return mpRect.h;
}



void Line::setSpeed(int speed) {
	mpSpeed = speed;
	return;
}
int Line::getSpeed() {

	return mpSpeed;
}

void Line::setColor(ofColor color){
	mpColor = color;
	return;
}


