#include "ofApp.h"
#include <ctime>
#include "includes.h"
#include "Utils.h"
#include "Line.h"
#include "Score.h"

const clock_t begin_time = clock();
clock_t old_time;
clock_t new_time;

unsigned int global_delta_time;
int puntuacion_1 = 0;
int puntuacion_2 = 0;
int totalScore;
bool endOfGame = false;


//std::string mpWinner;

bool stopGame = false;

bool key_down[255];

void Save() {
	int savedScore;
	if(puntuacion_1 > puntuacion_2){
		savedScore = puntuacion_1;
	}
	else {
		savedScore = puntuacion_2;
	}

	std::fstream file;

	file.open("score.sav", std::ios::out | std::ios::trunc | std::ios::binary);
	if (savedScore > totalScore) {
		totalScore = savedScore;
		file.write((char*)&savedScore, sizeof(int));
	}
	file.close();
	return;
}

void loadScore() {
	std::fstream file;
	file.open("score.sav", std::ios::in | std::ios::binary);
	if (!file.is_open()) {
		totalScore = 0;
		file.close();
		return;
	}
	else {
		file.read((char*)&totalScore, sizeof(int));
		return;
	}
}


//--------------------------------------------------------------
void ofApp::setup() {//Una vez al inicio = create
	loadScore();

	for (int i = 0; i < 255; i++)
	{
		key_down[i] = false;
	}

	ofSetFrameRate(60);//Max FPS del juego
	old_time = begin_time;
	new_time = begin_time;

	int paddle_w = 20;
	int paddle_h = 125;
	int ball_size = 18;
	int line_w = 10;
	int line_h = 2000;
	int score = 0;


	//Creaci�n de pala 1
	mpPaddle1 = new Paddle(20, SCREEN_HEIGHT / 2 - paddle_h / 2, paddle_h, paddle_w);
	mpPaddle1->setSpeed(0);

	//Creaci�n de pala 2
	mpPaddle2 = new Paddle(SCREEN_WIDTH - paddle_w - 20, SCREEN_HEIGHT / 2 - paddle_h / 2, paddle_h, paddle_w);
	mpPaddle2->setSpeed(0);

	//Creaci�n de ball
	mpBall = new Ball((SCREEN_WIDTH - ball_size) / 2, (SCREEN_HEIGHT - ball_size) / 2, ball_size, ball_size);
	mpBall->setSpeedX(300);
	mpBall->setSpeedY(0);

	//Creaci�n de l�nea inferior
	mpLine1 = new Line(SCREEN_HEIGHT - line_w - 45, SCREEN_WIDTH / 2 - line_h / 2, line_h, line_w);
	mpLine1->setSpeed(0);		//Line(int x, int y, int h, int w)

	mpScore = new Score(175, 70, 0, 0);
	mpScore2 = new Score(625, 70, 0, 0);

	mpWinner1 = new ofTrueTypeFont();
	mpWinner1->loadFont("coders_crux.ttf", 65);

	mpWinner2 = new ofTrueTypeFont();
	mpWinner2->loadFont("coders_crux.ttf", 65);







}

//--------------------------------------------------------------
void ofApp::update() {//Cada frame = step
	old_time = new_time;
	new_time = clock() - begin_time;
	//Tiempo por frame, el juego ira igual en un ordenador lento o rapido
	global_delta_time = int(new_time - old_time);

	if (key_down['r'])		//Reinicio del juego cuando alguien gana
	{
		stopGame = false;
		puntuacion_1 = 0;
		puntuacion_2 = 0;
		
	}

	if (stopGame) {
		return;
	}

	//----------------------------------------------------------
	mpPaddle1->setSpeed(0);
	mpPaddle2->setSpeed(0);
	if (key_down['w'])
	{
		mpPaddle1->setSpeed(-500);
	}
	if (key_down['s'])
	{
		mpPaddle1->setSpeed(500);
	}
	if (key_down['o'])
	{
		mpPaddle2->setSpeed(-500);
	}
	if (key_down['l'])
	{
		mpPaddle2->setSpeed(500);
	}


	//----------------------------------------------------------
	//Update de las palas
	mpPaddle1->update();
	mpPaddle2->update();


	//Comprobar colision en x
	bool colPaddle1 = C_RectangleCollision(mpPaddle1->getRect(), mpBall->getRect());
	bool colPaddle2 = C_RectangleCollision(mpPaddle2->getRect(), mpBall->getRect());

	int ball_x = mpBall->getX();
	int ball_w = mpBall->getW();
	int ball_y = mpBall->getY();
	int ball_h = mpBall->getH();
	int ball_middle = ball_y + ball_h / 2;

	if (colPaddle1) {
		int paddle_x = mpPaddle1->getX();
		int paddle_w = mpPaddle1->getW();
		int paddle_y = mpPaddle1->getY();
		int paddle_h = mpPaddle1->getH();
		if (ball_x > paddle_x + paddle_w / 2) {
			mpBall->setX(paddle_x + paddle_w);
			mpBall->invertSpdX();
		}

		if (ball_middle < paddle_y + paddle_h / 3) {
			mpBall->setSpeedY(-350);
		}
		else if (ball_middle > paddle_y + 2 * paddle_h / 3) {
			mpBall->setSpeedY(350);
		}
		else {
			mpBall->setSpeedY(0);
		}

		//Mirar que parte de la pala toca
		//Modificar spdY
	}
	if (colPaddle2) {
		int paddle_x = mpPaddle2->getX();
		int paddle_w = mpPaddle2->getW();
		int paddle_y = mpPaddle2->getY();
		int paddle_h = mpPaddle2->getH();
		if (ball_x + ball_w < paddle_x + paddle_w / 2) {
			mpBall->setX(paddle_x - ball_w);
			mpBall->invertSpdX();
		}
		//Mirar que parte de la pala toca
		//Modificar spdY
		if (ball_middle < paddle_y + paddle_h / 3) {
			mpBall->setSpeedY(-350);
		}
		else if (ball_middle > paddle_y + 2 * paddle_h / 3) {
			mpBall->setSpeedY(350);
		}
		else {
			mpBall->setSpeedY(0);
		}
	}


	bool out_of_screen = false;
	//Mirar si choca con los bordes laterale


	if (ball_x + ball_w < 0) {//Sale por la izquierda
		//Aumentar puntacion jugador 2
		out_of_screen = true;
		puntuacion_2 = puntuacion_2 + 1;
		mpScore2->setValue(puntuacion_2);

		if (puntuacion_2 == puntuacion_1 + 5) {
			stopGame = true;
		}
	}

	if (ball_x > SCREEN_WIDTH) {
		//Aumentar puntacion jugador 2
		out_of_screen = true;
		puntuacion_1 = puntuacion_1 + 1;
		mpScore->setValue(puntuacion_1);

		if (puntuacion_1 == puntuacion_2 + 5) {
			stopGame = true;
			
		}
	}

	if (out_of_screen) {
		mpBall->setXY((SCREEN_WIDTH - ball_w) / 2, (SCREEN_HEIGHT - ball_w) / 2);
		mpBall->invertSpdX();
	}

	//Mirar si toca con los bordes superiores

	//Update ball

	out_of_screen = false;
	if (ball_y < 0) {//Sale por arriba
		out_of_screen = true;

	}
	if (ball_y + ball_h > SCREEN_HEIGHT) {
		out_of_screen = true;
		//Aumentar puntacion jugador 1

	}
	if (out_of_screen) {
		mpBall->invertSpdY();

	}


	mpBall->update();

	if (!endOfGame) {
		if (puntuacion_2 == puntuacion_1 + 5)
		{
			Save();
		}else
			if(puntuacion_1 == puntuacion_2 + 5)
			{
				Save();
			}

}



}

//--------------------------------------------------------------
void ofApp::draw() {
	ofClear(0, 0, 0);

	mpLine1->render();
	mpPaddle1->render();
	mpPaddle2->render();
	mpBall->render();
	mpScore->render();
	mpScore2->render();
	if (puntuacion_2 == puntuacion_1 + 5) {
		mpWinner1->drawString("PLAYER2 IS THE WINNER!", 95, 200);	//Gana el jugador 1
	}
	else if (puntuacion_1 == puntuacion_2 + 5) {
		mpWinner2->drawString("PLAYER1 IS THE WINNER!", 95, 200);		//Gana el jugador 2
	}

}

//--------------------------------------------------------------
void ofApp::keyPressed(int key) {
	if (key >= 255 || key < 0) { return; }
	key_down[key] = true;
}

//--------------------------------------------------------------
void ofApp::keyReleased(int key) {
	if (key >= 255 || key < 0) { return; }
	key_down[key] = false;
}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y) {

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button) {

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button) {

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button) {

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y) {

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y) {

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h) {

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg) {

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo) {

}
