#pragma once

#include "ofMain.h"
#include "Paddle.h"
#include "ball.h"
#include "Line.h"
#include "Score.h"
class ofApp : public ofBaseApp{

	public:
		void setup();
		void update();
		void draw();

		void keyPressed(int key);
		void keyReleased(int key);
		void mouseMoved(int x, int y );
		void mouseDragged(int x, int y, int button);
		void mousePressed(int x, int y, int button);
		void mouseReleased(int x, int y, int button);
		void mouseEntered(int x, int y);
		void mouseExited(int x, int y);
		void windowResized(int w, int h);
		void dragEvent(ofDragInfo dragInfo);
		void gotMessage(ofMessage msg);

	private:
		Paddle* mpPaddle1;  //Pala jugador uno
		Paddle* mpPaddle2;  //Pala jugador dos

		Ball* mpBall;       //Pelota
		Line* mpLine1;     //Line inferior
		Line* mpLine2;	   //Line superior
		Score* mpScore;   //Puntuación jugador izquierda
		Score* mpScore2;  //Puntuación jugador derecha
		//Score* mpWinner1; //Gana el jugador 1
		//Score* mpWinner2; //Gana el jugador 2
		ofTrueTypeFont* mpWinner1;	//Ha ganado el jugador 1
		ofTrueTypeFont* mpWinner2;	//Ha ganado el jugadaor 2
};	
