#ifndef PADDLE_H
#define PADDLE_H

#include "includes.h"
#include "ofMain.h"

class Paddle
{
	public:
		Paddle();
		Paddle(int x, int y,int h, int w);
		~Paddle();

		void update();
		void render();

		//Setters and getters
		void setRect(C_Rectangle a_rect);
		void setXY(int x, int y);
		void setX(int x);
		void setY(int y);
		void setW(int w);
		void setH(int h);

		C_Rectangle getRect();
		int getX();
		int getY();
		int getW();
		int getH();



		void setSpeed(int Speed);
		int getSpeed();

		void setColor(ofColor color);
			

	protected:
	
	private:
		C_Rectangle mpRect; //Atributo rectangulo de la pala
		int mpSpeed;        //Velocidad de la pala
		ofColor mpColor;      //Color de la pala
	
};

#endif
