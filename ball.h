#ifndef BALL_H
#define BALL_H

#include "includes.h"
#include "ofMain.h"

class Ball
{
	public:
		Ball();
		Ball(int x, int y,int h, int w);
		~Ball();

		void update();
		void render();

		//Setters and getters
		void setRect(C_Rectangle a_rect);
		void setXY(int x, int y);
		void setX(int x);
		void setY(int y);
		void setW(int w);
		void setH(int h);

		C_Rectangle getRect();
		int getX();
		int getY();
		int getW();
		int getH();



		void setSpeedX(int speed_x);
		void setSpeedY(int speed_y);
		void invertSpdX();
		void invertSpdY();
		int getSpeedX();
		int getSpeedY();

		void setColor(ofColor color);
			

	protected:
	
	private:
		C_Rectangle mpRect;	   //Atributo rectangulo de la pala
		int mpSpeedX;         //Velocidad de la pala
		int mpSpeedY;		 //Velocidad bola en y
		ofColor mpColor;    //Color de la pala
	
};

#endif
