//Include our classes
#include "Paddle.h"

Paddle::Paddle() {
	mpRect.x = 0;
	mpRect.y = 0;
	mpRect.h = 0;
	mpRect.w = 0;
	mpSpeed = 0;
	mpColor = ofColor(254, 000, 000);
}

Paddle::Paddle(int x, int y, int h, int w) :Paddle() {
	setXY(x, y);
	setW(w);
	setH(h);
}

Paddle::~Paddle() {  //Solo tiene que tener cosas si hay atributos que canvien de tama�o, los atributos constantes no.

}

void Paddle::update() {
	mpRect.y = mpRect.y + mpSpeed * (int)global_delta_time / 1000;
	if (mpRect.y < 0)
	{
		mpRect.y = 0;
	}
	if (mpRect.y + mpRect.h > SCREEN_HEIGHT)
	{
		mpRect.y = SCREEN_HEIGHT - mpRect.h;
	}
	return;
}

void Paddle::render() {
	ofSetColor(mpColor);
	ofDrawRectangle(mpRect.x, mpRect.y, mpRect.w, mpRect.h);
	return;
}

//Setters and getters
void Paddle::setRect(C_Rectangle a_rect) {
	setXY(a_rect.x, a_rect.y);
	setW(a_rect.w);
	setH(a_rect.h);
	return;
}
void Paddle::setXY(int x, int y) {
	setX(x);
	setY(y);
	return;
}
void Paddle::setX(int x) {
	mpRect.x = x;
	return;
}
void Paddle::setY(int y) {
	mpRect.y = y;
	return;
}
void Paddle::setW(int w) {
	mpRect.w = w;
	return;
}
void Paddle::setH(int h) {
	mpRect.h = h;
	return;
}

//Gets
C_Rectangle Paddle::getRect() {

	return mpRect;
}
int Paddle::getX() {

	return mpRect.x;
}
int Paddle::getY() {

	return mpRect.y;
}
int Paddle::getW() {

	return mpRect.w;
}
int Paddle::getH() {

	return mpRect.h;
}



void Paddle::setSpeed(int speed) {
	mpSpeed = speed;
	return;
}
int Paddle::getSpeed() {

	return mpSpeed;
}

void Paddle::setColor(ofColor color){
	mpColor = color;
	return;
}


