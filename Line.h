#ifndef Line_H
#define Line_H

#include "includes.h"
#include "ofMain.h"

class Line
{
	public:
		Line();
		Line(int x, int y,int h, int w);
		~Line();

		void update();
		void render();

		//Setters and getters
		void setRect(C_Rectangle a_rect);
		void setXY(int x, int y);
		void setX(int x);
		void setY(int y);
		void setW(int w);
		void setH(int h);

		C_Rectangle getRect();
		int getX();
		int getY();
		int getW();
		int getH();



		void setSpeed(int Speed);
		int getSpeed();

		void setColor(ofColor color);
			

	protected:
	
	private:
		C_Rectangle mpRect; //Atributo rectangulo de la pala
		int mpSpeed;        //Velocidad de la pala
		ofColor mpColor;      //Color de la pala
	
};

#endif
